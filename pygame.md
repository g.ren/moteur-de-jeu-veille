# PyGame

| Caractéristiques | |
| :--------------- | :--------------- | 
| 2D               | Oui |
| Multiplatforme   | Supporté sur Windows, MacOS et GNU/Linux |
| Réseau           | Supporte le réseau, mais la communication ne se fait pas que par PyGame |
| Son              | Contient un module de son, permettant au jeu d'utiliser un mixeur pour les sons du jeu |
| Gratuit          | Oui, c'est une librairie Python |
| License          | GNU LGPL-2.1 |
| Python           | PyGame n'est à proprement parler pas directement un moteur de jeu, mais une librairie permettant d'en créer (ce qui donne pas mal de possibilités, comme la combiner avec d'autres) qui est donc développée pour Python. Elle est développé avec le language C, permettant donc d'implémenter divers technologies qui ne sont pas utilisables dans Python par défaut pour la création de jeux. |
