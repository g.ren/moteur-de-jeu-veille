# Conventions de Création d'Informations sur les Moteurs et Bibliothèques Graphiques

## First Step

La première étape est de créer un fichier markdown (extension de fichier en `.md`). Le nom de fichier doit être entièrement fait de minuscules et ne doit contenir que des caractères ASCII classiques (a-z et 0-9). Pour représenter un espace, utilisez le caractère `"-"`.

## Final Step

La seconde étape est de copier le contenu du fichier `convention-template.md`.

Vous devirez avoir une représentation telle que celle-ci dans votre fichier :

`my-library-info.md`
```
# Template

| Caractéristiques |                  |
| :--------------- | :--------------- | 
| 2D               | ---------------- |
| Multiplatforme   | ---------------- |
| Réseau           | ---------------- |
| Son              | ---------------- |
| Gratuit          | ---------------- |
| License          | ---------------- |
| Python           | ---------------- |
```

Ce fichier génère un tableau à l'affichage par l'utilisation des conventions markdown. Ainsi donc, il vous suffit de remplacer le `Template` par le nom de votre moteur de jeu / bibliothèque et les suites de `-` par les informations correspondantes (attention pas les deux premières suites qui commencent par un `:`, ces dernières doivent rester, c'est grâce à elles que le tableau est généré).

Et voilà ! Votre fichier et terminé et suit correctement les conventions !